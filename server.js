const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
config = require('./config/DB');
const mongoose = require('mongoose');
global.__root   = __dirname + '/';

// API file for interacting with MongoDB
const api = require('./server/routes/api');

mongoose.Promise = global.Promise;
mongoose.connect(config.DB).then(
  () => {console.log('Database is connected') },
err => { console.log('Can not connect to the database'+ err)}
);

//

var UserController = require(__root + 'server/UserController');
app.use('/api/usersauth', UserController);

var AuthController = require(__root + 'server/AuthController');
app.use('/api/auth', AuthController);


app.use(express.static(path.join(__dirname, 'public')));

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

// API location
app.use('/api', api);

// Send all other requests to the Angular app
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//Set Port
const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => console.log(`Running on localhost:${port}`));


