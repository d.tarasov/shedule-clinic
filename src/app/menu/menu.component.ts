import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  title = 'Расписание для клиники';

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  logout(): void {
    this.dataService.logout().subscribe();
  }

}
