import {Component, OnInit} from '@angular/core';
import {DataService} from "../data.service";
import {User} from "../user";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  token: string;
  test: string;

  loginForm: FormGroup;

  constructor(private dataService: DataService, private router: Router) {
    this.loginForm = new FormGroup({
      "email": new FormControl('', Validators.email),
      "password": new FormControl('', Validators.required)
    });
    this.test = localStorage.getItem('token');
  }

  ngOnInit() {

  }

  login(email, password: string): void {
    if (!email && !password) {
      return;
    }
    const newUser: User = { email, password } as User;
    this.dataService.login(email, password).subscribe(res => {
      this.token = res.token;
      this.router.navigate(['/calendar']);
      console.log(this.token);
      localStorage.setItem('token', this.token);
      console.log(localStorage.getItem('token'))});
  }

  me(): void {
  }

  logout(): void {
    this.dataService.logout().subscribe();
  }

  submit() {
    this.login(this.loginForm.value.email, this.loginForm.value.password);
  };

}
