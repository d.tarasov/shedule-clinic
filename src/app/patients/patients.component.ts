import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";
import {User} from "../user";

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.css']
})
export class PatientsComponent implements OnInit {
  patients: User[];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.getPatients();
  }

  getPatients(): void {
    this.dataService.getPatients()
      .subscribe(patient => this.patients = patient);
  }

  add(surname, name: string): void {
    if (!surname && !name) {return;
    }
    const role = 'patient';
    // The server will generate the id for this new patient
    const newUser: User = { surname, name, role } as User;
    this.dataService.addPatient(newUser)
      .subscribe(user => this.patients.push(newUser));
    this.getPatients();
  }

  delete(id) {
    this.patients = this.patients.filter(p => p._id !== id);
    this.dataService.deleteUser(id).subscribe();
  }

}
