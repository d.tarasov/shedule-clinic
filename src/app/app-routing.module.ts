import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DoctorsComponent} from "./doctors/doctors.component";
import {PatientsComponent} from "./patients/patients.component";
import {UsersComponent} from "./users/users.component";
import {FullcalendarComponent} from "./fullcalendar/fullcalendar.component";
import {AddsheduleComponent} from "./addshedule/addshedule.component";
import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";
import {MenuComponent} from "./menu/menu.component";
import {AuthGuard} from "./guard/auth.guard";

const routes: Routes = [
  { path: 'doctors', component: DoctorsComponent, canActivate: [AuthGuard] },
  { path: 'patients', component: PatientsComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  {path: 'calendar', component: FullcalendarComponent, canActivate: [AuthGuard]},
  {path: 'shedule', component: AddsheduleComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent },
  {path: 'registration', component: RegistrationComponent },
  {path: 'menu', component: MenuComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: '/calendar'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
