import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AuthService implements HttpInterceptor {
  public token = "";

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('auth');
    if(localStorage.getItem('token') != null)
    this.token = localStorage.getItem('token');
    const authReq = req.clone({
      headers: req.headers.set('x-access-token', this.token)
    });

    console.log(req);
    return next.handle(authReq);
  }

}
