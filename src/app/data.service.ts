import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {User} from "./user";
import {Shedule} from "./shedule";
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders} from "@angular/common/http";

var token = 'TOKEN';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};





@Injectable()
export class DataService {

  result: any;





  private usersUrl = 'api/saveuser'; // URL to web api

  constructor(private http: HttpClient) { }



  /*getUsers() {
    return this.http.get("/api/users")
      .map(result => this.result = result.json().data);
  }*/

  getDoctors (): Observable<User[]> {
    return this.http.get<User[]>('/api/doctors');
  }

  getPatients (): Observable<User[]> {
    return this.http.get<User[]>('/api/patients');
  }

  /** GET users from the server */
  getUsers1() {
    return this.http.get('/api/users')
      .map(res => {
        return res;
      });
  }

  /** GET users from the server */
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('/api/users');
  }

  /** TEST */
  test(): Observable<User> {
    return this.http.get<User>('/api/auth/me');
  }

  /** GET shedules from the server */
  getShedules(): Observable<Shedule[]> {
    return this.http.get<Shedule[]>('/api/shedules');
  }

  //////// Save methods //////////

  /** POST: add a new user to the database */
  /**(методы работы с USER будут заменены на методы из UserController.js */
  addUser (user: User): Observable<any> {
    return this.http.post<User>('/api/auth/register', user, httpOptions);
  }

  /** POST: add a new doctor to the database */
  /**(методы работы с USER будут заменены на методы из UserController.js */
  addDoctor (user: User): Observable<any> {
    return this.http.post<User>('/api/saveuser', user, httpOptions);
  }

  /** POST: add a new patient to the database */
  /**(методы работы с USER будут заменены на методы из UserController.js */
  addPatient (user: User): Observable<any> {
    return this.http.post<User>('/api/saveuser', user, httpOptions);
  }

  addShedule (shedule: Shedule): Observable<Shedule> {
    return this.http.post<Shedule>('/api/addshedule', shedule, httpOptions);
  }

  deleteUser(id) {
    console.log('delete' + id);
    return this.http.delete('/api/deleteuser/' + id);
  }

  deleteShedule(id) {
    console.log('delete' + id);
    return this.http.delete('/api/deleteshedule/' + id);
  }

  login(email, password): Observable<any> {
    console.log('dataservice');
    const body = {email: email, password: password};
    console.log(body);
    return this.http.post('/api/auth/login', body, httpOptions);
  }

  logout(): Observable<any> {
    localStorage.removeItem('token');
    return this.http.get('/api/auth/logout').map(res => console.log(res));
  }

  login1 (user: User): Observable<User> {

    return this.http.post<User>('/api/auth/login', user, httpOptions);
  }

}
