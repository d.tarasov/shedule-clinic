import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { DoctorsComponent } from './doctors/doctors.component';
import { PatientsComponent } from './patients/patients.component';
import {HttpModule} from "@angular/http";
import {DataService} from "./data.service";
import {HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './/app-routing.module';
import { UsersComponent } from './users/users.component';
import { FullcalendarComponent } from './fullcalendar/fullcalendar.component';
import { AddsheduleComponent } from './addshedule/addshedule.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { MenuComponent } from './menu/menu.component';
import { ReactiveFormsModule} from "@angular/forms";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {AuthService} from "./interceptors/auth.service";
import {AuthGuard} from "./guard/auth.guard";


@NgModule({
  declarations: [
    AppComponent,
    DoctorsComponent,
    PatientsComponent,
    UsersComponent,
    FullcalendarComponent,
    AddsheduleComponent,
    LoginComponent,
    RegistrationComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [DataService, AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthService,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
