import {Component, OnInit} from '@angular/core';
import {DataService} from "../data.service";
import {User} from "../user";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.css']
})
export class DoctorsComponent implements OnInit {
  doctors: User[];
  doctorsForm: FormGroup;

  constructor(private dataService: DataService) {
    this.doctorsForm = new FormGroup({
      "surname": new FormControl('', Validators.required),
      "name": new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.getDoctors();
  }

  getDoctors(): void {
    this.dataService.getDoctors()
      .subscribe(doctor => this.doctors = doctor);
  }

  add(surname, name: string): void {
    if (!surname && !name) {
      return;
    }
    const role = 'doctor';
    // The server will generate the id for this new doctor
    const newUser: User = {surname, name, role} as User;
    this.dataService.addDoctor(newUser)
      .subscribe(user => this.doctors.push(newUser));
    this.getDoctors();
  }

  delete(id) {
    this.doctors = this.doctors.filter(d => d._id !== id);
    this.dataService.deleteUser(id).subscribe();
  }

  submit() {
    this.add(this.doctorsForm.value.surname, this.doctorsForm.value.name);
  };

}
