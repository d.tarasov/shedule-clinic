export interface User {
  _id: string;
  email: string;
  password: string;
  surname: string;
  name: string;
  role: string;
}
