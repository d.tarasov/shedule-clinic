export interface Shedule {
  _id: string;
  start: string;
  end: string;
  title: string;
}
