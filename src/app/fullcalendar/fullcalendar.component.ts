import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import 'fullcalendar';
import {User} from "../user";
import {DataService} from "../data.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-fullcalendar',
  templateUrl: './fullcalendar.component.html',
  styleUrls: ['./fullcalendar.component.css']
})
export class FullcalendarComponent implements OnInit {


  constructor(private dataService: DataService, private router: Router) {
  }

  doctors: User[];
  patients: User[];

  ngOnInit() {

    const that = this;
    this.getDoctors();
    this.getPatients();


    /*$(document).ready(function() {

      // page is now ready, initialize the calendar...

      $('#calendar').fullCalendar({
        header: { center: 'month,agendaWeek' }, // buttons for switching between views

        views: {
          month: { // name of view
            titleFormat: 'YYYY, MM, DD'
            // other view-specific options here
          }
        }
      });

    });*/

    $(function() {
      let containerEl: JQuery = $('#calendar');

      containerEl.fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay,listWeek'
        },
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events


        eventSources: [{
          url: 'api/shedules'
        }],

        eventClick: function(calEvent, jsEvent, view) {
          alert('Запись: ' + calEvent.title);
        },
        dayClick: function(calEvent, jsEvent, view) {
          that.router.navigate(['/shedule']);
        }
      })
    });
  }

  getDoctors(): void {
    this.dataService.getDoctors()
      .subscribe(doctors => this.doctors = doctors);
  }

  getPatients(): void {
    this.dataService.getPatients()
      .subscribe(patietns => this.patients = patietns);
  }


}
