import { Component, OnInit } from '@angular/core';
import {User} from "../user";
import {DataService} from "../data.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  providers: [ DataService ],
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];

  roleT = ['doctor', 'patient'];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.dataService.getUsers()
      .subscribe(users => this.users = users);
  }

/*  add1(username, password: string): void {
    username = username.trim();
    if (!username) {
      return;
    }
    this.dataService.addUser({surname: surname, name: name, role: role} as User)
      .subscribe(user => {
        this.users.push(user);
      });
  }*/

  add(email, password, surname, name: string): void {
    if (!surname && !name) {return;
    }
    // The server will generate the id for this new hero
    const role = 'admin';
    const newUser: User = { email, password, surname, name, role } as User;
    this.dataService.addUser(newUser)
      .subscribe(user => this.users.push(newUser));
    this.getUsers();
  }

  delete(id) {
    this.users = this.users.filter(u => u._id !== id);
    this.dataService.deleteUser(id).subscribe();
  }

}
