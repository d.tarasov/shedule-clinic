import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsheduleComponent } from './addshedule.component';

describe('AddsheduleComponent', () => {
  let component: AddsheduleComponent;
  let fixture: ComponentFixture<AddsheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
