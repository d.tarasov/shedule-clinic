import { Component, OnInit } from '@angular/core';
import {Shedule} from "../shedule";
import {DataService} from "../data.service";
import {User} from "../user";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-addshedule',
  templateUrl: './addshedule.component.html',
  styleUrls: ['./addshedule.component.css']
})
export class AddsheduleComponent implements OnInit {

  shedules: Shedule[];
  doctors: User[];
  patients: User[];


  constructor(private dataService: DataService) { }

  ngOnInit(): void{
    this.getDoctors();
    this.getPatients();
    this.getShedules();

  }

  getDoctors(): void {
    this.dataService.getDoctors()
      .subscribe(doctors => this.doctors = doctors);
  }

  getPatients(): void {
    this.dataService.getPatients()
      .subscribe(patietns => this.patients = patietns);
  }

  getShedules(): void {
    this.dataService.getShedules()
      .subscribe(shedule => this.shedules = shedule);
  }

  add(start_date, start_time, end_time, doctor_surname, patient_surname: string): void {
    if (!start_date) {return;
    }
    // The server will generate the id for this new shedule
    console.log(patient_surname);
    const start = start_date + 'T' + start_time + 'Z';
    const end = start_date + 'T' + end_time + 'Z';
    const title = "Доктор: " + doctor_surname + "/" + "Пациент: " + patient_surname;
    const newShedule: Shedule = { start, end, title } as Shedule;
    this.dataService.addShedule(newShedule)
      .subscribe(shedule => this.shedules.push(newShedule));
    this.getShedules();
  }

  delete(id) {
    this.shedules = this.shedules.filter(d => d._id !== id);
    this.dataService.deleteShedule(id).subscribe();
  }



}
