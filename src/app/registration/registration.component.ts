import { Component, OnInit } from '@angular/core';
import {User} from "../user";
import {DataService} from "../data.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  token: string;
  registrationForm: FormGroup;

  constructor(private dataService: DataService, private router: Router) {
    this.registrationForm = new FormGroup({
      "email": new FormControl('', Validators.email),
      "password": new FormControl('', Validators.minLength(1)),
      "surname": new FormControl('', Validators.minLength(2)),
      "name": new FormControl('', Validators.minLength(2))
    });
  }

  ngOnInit() {
    this.token = '';
  }

  registration(email, password, surname, name: string): void {
    console.log('r');
    if (!surname && !name && !password && !email) {return;
    }
    const role = 'admin';
    // The server will generate the id for this new user
    const newUser: User = { email, password, surname, name, role } as User;
    this.dataService.addUser(newUser)
      .subscribe(res => {this.token = res.token; console.log(this.token); localStorage.setItem('token', this.token); console.log(localStorage.getItem('token'));
        this.router.navigate(['/login']);
      });
  }

  test(): void {
    console.log('test');
    this.dataService.test().subscribe();
  }

  submit() {
    this.registration(this.registrationForm.value.email, this.registrationForm.value.password, this.registrationForm.value.surname, this.registrationForm.value.name);
  };

}
