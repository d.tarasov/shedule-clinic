var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Shedule = new Schema({
  start: {type: String},
  end: {type: String},
  title: {type: String}

}, {
  collection: 'shedules'
});

module.exports = mongoose.model('Shedule', Shedule);
