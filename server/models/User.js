var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = new Schema({
  email: {type: String},
  password: {type: String},
  surname: {type: String},
  name: {type: String},
  role: {type: String}
}, {
  collection: 'users'
});

module.exports = mongoose.model('User', User);
