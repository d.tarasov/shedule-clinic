const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
var User = require('../models/User');
var Shedule = require('../models/Shedule');
var VerifyToken = require('../VerifyToken');

// Connect
/*const connection = (closure) => {
 return MongoClient.connect('mongodb://127.0.0.1:27017/sheduleclinic', (err, db) => {
 if (err) return console.log(err);

 closure(db);
 });
 };*/

// Error handling
const sendError = (err, res) => {
  response.status = 501;
  response.message = typeof err == 'object' ? err.message : err;
  res.status(501).json(response);
};

// Response handling
/*let response = {
 status: 200,
 data: [],
 message: null
 };*/

// Response handling
let response = {};

// Get users
router.route('/users').get(VerifyToken, function (req, res) {
  User.find({role: /admin/}).exec(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

// Get doctors
router.route('/doctors').get(function (req, res) {
  User.find({role: /doctor/}).exec(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

// Get patients
router.route('/patients').get(function (req, res) {
  User.find({role: /patient/}).exec(function (err, users){
    if(err){
      console.log(err);
    }
    else {
      res.json(users);
    }
  });
});

// Get shedules
router.route('/shedules').get(function (req, res) {
  Shedule.find(function (err, shedules){
    if(err){
      console.log(err);
    }
    else {
      res.json(shedules);
    }
  });
});

/* DELETE User */
router.route('/deleteuser/:id')
  .delete(function(req, res) {
    User.findByIdAndRemove({_id: req.params.id}, function(err, user) {
      if (err)
        res.next(err);
      else
        res.json({ message: 'Successfully deleted' });
    });
  });

/* DELETE Shedule */
router.route('/deleteshedule/:id')
  .delete(function(req, res) {
    Shedule.findByIdAndRemove({_id: req.params.id}, function(err, user) {
      if (err)
        res.next(err);
      else
        res.json({ message: 'Successfully deleted' });
    });
  });

/*router.route('/deleteuser/:id').get(function(req, res){
 r = req.params.id;
 res.status(200).send("pong1!" + r);
 });*/

//SAVE USER
router.route('/saveuser').post(function (req, res) {
  var user = new User(req.body);
  user.save()
    .then(item => {
    res.status(200).json({'coin': 'Coin added successfully'});
})
  .catch(err => {
    res.status(400).send("unable to save to database");
});
});

router.route('/addshedule').post(function (req, res) {
  var shedule = new Shedule(req.body);
  shedule.save()
    .then(item => {
    res.status(200).json({'coin': 'Coin added successfully'});
})
  .catch(err => {
    res.status(400).send("unable to save to database");
});
});

//Save user
router.post('/saveuser1', (req, res) => {
  connection((db) => {
  db.collection('users').insertOne()
})
})

/*router.get('/ping', function(req, res){
 res.status(200).send("pong!");
 });*/

router.route('/ping').get(function(req, res){
  res.status(200).send("pong!");
});

/*//Authenticate

 router.get('/', (req, res) => {
 res.render('index', { user : req.user });
 });

 router.get('/register', (req, res) => {
 res.render('register', { });
 });

 router.post('/register', (req, res, next) => {
 Account.register(new Account({ username : req.body.username }), req.body.password, (err, account) => {
 if (err) {
 return res.render('register', { error : err.message });
 }

 passport.authenticate('local')(req, res, () => {
 req.session.save((err) => {
 if (err) {
 return next(err);
 }
 res.redirect('/');
 });
 });
 });
 });


 router.get('/login', (req, res) => {
 res.render('login', { user : req.user, error : req.flash('error')});
 });

 router.post('/login', passport.authenticate('local', { failureRedirect: '/login', failureFlash: true }), (req, res, next) => {
 req.session.save((err) => {
 if (err) {
 return next(err);
 }
 res.redirect('/');
 });
 });

 router.get('/logout', (req, res, next) => {
 req.logout();
 req.session.save((err) => {
 if (err) {
 return next(err);
 }
 res.redirect('/');
 });
 });*/

module.exports = router;
